// 1. Import express using required directive.
const express = require('express');

// 2. Initialize express by using it to the app variable as a function.
const app = express();

// 3. Set the port number
const port = 4004;

// 4. Use middleware to allow express to read JSON
app.use(express.json());

// 5. Use middleware to allow express to be able to read more data types from a response.
app.use(express.urlencoded({extended: true}));

// 6. Listen to the port and console.log a text once the server is running.
app.listen(port, () => console.log(`Server is running at localhost:${port}`));

// [SECTION] Routes
    // Express has methods corresponding to each HTTP method.

app.get('/hello', (req, res) => {
    res.send(`Hello from the /hello endpoint.`);
})

app.post('/display-name', (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

    // [SUB-SECTION] Sign-Up Form

    let users = [];

    app.post('/sign-up', (req, res) => {
        if (req.body.username !== '' && req.body.password !== '') {
            users.push(req.body);
            res.send(`User ${req.body.username} successfully registered!`);
        } else {
            res.send('Please input BOTH username and password!');
        }
    });

    app.put('/change-password', (req, res) => {

        // Creates a variable to store the message to be sent back to the client/Postman
        let message = '';
    
        // Creates a for loop that will loop through the elements of the 'users' array.
        for (let i = 0; i < users.length; i++) {
            if (req.body.username == users[i].username) {

                // Changes the password of the user found by the loop into the password provided in the client/Postman
                users[i].password = req.body.password;
                message = `User ${req.body.username}'s password has been updated`;

                // Breaks iout of the loob once a user that matches the username provided in the client/Postman is found.
                break;

            // If no user was found 
            } else {

                // Chanes the message to be sent back by the response.
                message = 'User does not exist!';
            }
        }
    
        res.send(message);
    });

// Activity

// 1
app.get('/home', (req, res) => {

    res.send('Welcome to the homepage!');

});

// 2
app.get('/users', (req, res) => {

    if (users.lenth == 0) {
        
    } else {

        res.send(users);

    }
});

// 3
app.delete('/delete-user', (req, res) => {
    res.send(`User ${req.body.username} has been deleted.`)
});
   